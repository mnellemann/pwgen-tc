package biz.nellemann.pwgen.util;

public class Colors {

	// Blue Palette
	public static int BLUE_1 =  0x03254c;
	public static int BLUE_2 =  0x1167b1;
	public static int BLUE_3 =  0x187bcd;
	public static int BLUE_4 =  0x2a9df4;
	public static int BLUE_5 =  0xd0efff;

	// Gray Palette
	public static int GRAY_1 = 0x838486;
	public static int GRAY_2 = 0x999a9d;
	public static int GRAY_3 = 0xaeb0b3;
	public static int GRAY_4 = 0xc4c6ca;
	public static int GRAY_5 = 0xdadce0;

	// Primary Colors
	public static int PRIMARY = BLUE_2;
	public static int P_LIGHT = BLUE_2;
	public static int P_DARK = BLUE_1;

	// Secondary Colors
	public static int SECONDARY = BLUE_3;
	public static int S_LIGHT = BLUE_4;
	public static int S_DARK = BLUE_2;

	//Texts Colors
	public static int TEXT_ON_P = 0xFFFFFF;
	public static int TEXT_ON_P_LIGHT = 0x000000;
	public static int TEXT_ON_P_DARK = 0xFFFFFF;

	public static int TEXT_ON_S = 0x000000;
	public static int TEXT_ON_S_LIGHT = 0x000000;
	public static int TEXT_ON_S_DARK = 0xFFFFFF;

	// Backcolor samples colors
	public static int BACKGROUND_WHITE = 0xFFFFFF;
	public static int BACKGROUND_GRAY_01 = GRAY_1;
	public static int BACKGROUND_GRAY_02 = GRAY_2;
	public static int BACKGROUND_GRAY_03 = GRAY_3;
	public static int BACKGROUND_GRAY_04 = GRAY_4;

}
