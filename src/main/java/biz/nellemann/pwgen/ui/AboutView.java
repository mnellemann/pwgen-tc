package biz.nellemann.pwgen.ui;

import biz.nellemann.pwgen.util.Colors;
import biz.nellemann.pwgen.util.Fonts;
import biz.nellemann.pwgen.util.Images;
import biz.nellemann.pwgen.util.MaterialConstants;

import totalcross.sys.Vm;
import totalcross.ui.*;
import totalcross.ui.Container;


public class AboutView extends Container {

    private Bar bar;
    private Container body;

    public void initUI() {

        bar = new Bar("About");
        bar.setFont(Fonts.latoBoldPlus10);
        bar.setBackForeColors(Colors.BLUE_1, Colors.TEXT_ON_P);
        bar.setIcon(Images.logo);

        body = new Container();
        body.setBackColor(Colors.BACKGROUND_WHITE);

        add(bar, LEFT, TOP, FILL, MaterialConstants.EDIT_HEIGHT);
        add(body, LEFT, AFTER, FILL, FILL);

        Label labelAbout = new Label("About bla bla");
        body.add(labelAbout, CENTER, AFTER + MaterialConstants.FIFTY_DP);

        Button readMore = new Button("Read more about Password Generator", Button.BORDER_NONE);
        //readMore.transparentBackground = true;
        readMore.setFont(Fonts.latoRegularDefaultSize);
        readMore.setForeColor(Colors.SECONDARY);
        body.add(readMore, LEFT, AFTER + MaterialConstants.COMPONENT_SPACING, FILL, PREFERRED);
        readMore.addPressListener((e) -> {
            Vm.exec("url", "https://bitbucket.org/mnellemann/pwgen-tc/", 0, true);
        });


        FloatingActionButton floatbutton = new FloatingActionButton(Images.float_button_back_on_s);
        floatbutton.setBackColor(Colors.BLUE_3);
        floatbutton.setIconSize(10);
        floatbutton.setIcon(Images.float_button_back_on_s);
        add(floatbutton, RIGHT - MaterialConstants.COMPONENT_SPACING, BOTTOM - MaterialConstants.TWENTY_DP);

        floatbutton.addPressListener((e) -> {
            HomeView homeView = new HomeView();
            MainWindow.getMainWindow().swap(homeView);
        });

    }

}
