package biz.nellemann.pwgen.ui;

import biz.nellemann.libpw.PasswordGenerator;
import biz.nellemann.pwgen.util.Colors;
import biz.nellemann.pwgen.util.Fonts;
import biz.nellemann.pwgen.util.Images;
import biz.nellemann.pwgen.util.MaterialConstants;

import totalcross.ui.*;
import totalcross.ui.Button;
import totalcross.ui.Container;
import totalcross.ui.Label;


public class HomeView extends Container {
	
	private Bar bar;
	private Container body, containerOptions, containerLength, containerResult;
	private Edit editPassword;
	private Label labelLengthValue;

	private boolean useCapitals = true;
	private boolean useNumbers = false;
	private boolean useSymbols = false;
	private int passwordLength = 8;
	private static int counter = 0;

	public void initUI() {

		Images.loadImage();

		bar = new Bar("Password Generator");
		bar.setFont(Fonts.latoBoldPlus10);
		bar.setBackForeColors(Colors.BLUE_1, Colors.TEXT_ON_P);
		bar.setIcon(Images.logo);

		body = new Container();
		body.setBackColor(Colors.BACKGROUND_GRAY_01);

		add(bar, LEFT, TOP, FILL, MaterialConstants.EDIT_HEIGHT);
		add(body, LEFT, AFTER, FILL, FILL);

		FloatingActionButton floatbutton = new FloatingActionButton();
		floatbutton.setBackColor(Colors.BLUE_3);
		floatbutton.setIconSize(10);
		add(floatbutton, RIGHT - MaterialConstants.COMPONENT_SPACING, BOTTOM - MaterialConstants.TWENTY_DP);

		containerOptions = new Container();
		containerOptions.setBackColor(Colors.BACKGROUND_GRAY_02);
		body.add(containerOptions, LEFT, AFTER, FILL, 175);

		containerLength = new Container();
		containerLength.setBackColor(Colors.BACKGROUND_GRAY_03);
		body.add(containerLength, LEFT, AFTER, FILL, 175);

		containerResult = new Container();
		containerResult.setBackColor(Colors.BACKGROUND_GRAY_04);
		body.add(containerResult, LEFT, AFTER, FILL, 175);


		// Options

		Label labelOptions = new Label("Options");
		labelOptions.setFont(Fonts.latoBoldPlus6);
		labelOptions.setForeColor(Colors.BLUE_1);

		Check check1 = new Check("Include capitals");
		check1.setChecked(useCapitals);
		check1.addPressListener((e) -> { useCapitals = !useCapitals; });

		Check check2 = new Check("Include numbers");
		check2.setChecked(useNumbers);
		check2.addPressListener((e) -> { useNumbers = !useNumbers; });

		Check check3 = new Check("Include symbols");
		check3.setChecked(useSymbols);
		check3.addPressListener((e) -> { useSymbols = !useSymbols; });


		containerOptions.add(labelOptions, CENTER, TOP+MaterialConstants.TWENTY_DP);
		containerOptions.add(check1, LEFT, AFTER);
		containerOptions.add(check2, LEFT, AFTER);
		containerOptions.add(check3, LEFT, AFTER);

		// Length

		Label labelLength = new Label("Length");
		labelLength.setFont(Fonts.latoBoldPlus6);
		labelLength.setForeColor(Colors.BLUE_1);

		Slider slider = new Slider();
		slider.setBackColor(Colors.BLUE_5);
		slider.sliderColor = Colors.BLUE_2;
		slider.ticksColor = Colors.BLUE_1;
		slider.setValue(passwordLength);
		slider.setMinimum(4);
		slider.setMaximum(64);
		slider.drawTicks = true;
		slider.addPressListener((e) -> {
			Slider s = (Slider) e.target;
			passwordLength = s.getValue();
			labelLengthValue.setText(String.valueOf(passwordLength));
		});

		labelLengthValue = new Label(" " + String.valueOf(passwordLength) + " ");

		containerLength.add(labelLength, CENTER, TOP+MaterialConstants.TWENTY_DP);
		containerLength.add(slider, CENTER, AFTER + MaterialConstants.TWENTY_DP, SCREENSIZE - MaterialConstants.TWO_DP_SPACING, MaterialConstants.TWENTY_DP);
		containerLength.add(labelLengthValue, CENTER, AFTER);

		Button buttonGenerate = new Button("Generate");
		buttonGenerate.addPressListener((e) -> { generateRandomPassword(); });
		buttonGenerate.setBackColor(Colors.PRIMARY);
		buttonGenerate.setForeColor(Colors.TEXT_ON_P);
		containerResult.add(buttonGenerate, CENTER, TOP+MaterialConstants.TWENTY_DP, SCREENSIZE - MaterialConstants.TWO_DP_SPACING, MaterialConstants.FIFTY_DP);

		editPassword = new Edit();
		containerResult.add(editPassword, CENTER, AFTER + MaterialConstants.TWENTY_DP, SCREENSIZE - MaterialConstants.TWO_DP_SPACING, MaterialConstants.FIFTY_DP);


		floatbutton.addPressListener((e) -> {
			AboutView aboutView = new AboutView();
			MainWindow.getMainWindow().swap(aboutView);
		});

	}

	private void generateRandomPassword() {
		counter++;
		System.out.println("Caps: " + useCapitals);
		System.out.println("Nums: " + useNumbers);
		System.out.println("Syms: " + useSymbols);
		System.out.println("Length: " + passwordLength);

		String randomPassword = PasswordGenerator.random(passwordLength, useCapitals, useNumbers, useSymbols);
		//String randomPassword = passwordLength + " ranD0mP4ssWo3D " + counter;
		editPassword.setText(randomPassword);
	}

}
