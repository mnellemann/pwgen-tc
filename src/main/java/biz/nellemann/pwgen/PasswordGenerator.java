package biz.nellemann.pwgen;

import biz.nellemann.pwgen.util.Images;
import totalcross.ui.MainWindow;
import totalcross.sys.Settings;
import totalcross.io.IOException;
import totalcross.ui.image.ImageException;

import biz.nellemann.pwgen.ui.HomeView;
import biz.nellemann.pwgen.ui.SplashWindow;


public class PasswordGenerator extends MainWindow {
    
    public PasswordGenerator() {
        setUIStyle(Settings.MATERIAL_UI);
    }

    static {
		Settings.applicationId = "PWGEN";
		Settings.appVersion = "1.0.0";
		Settings.iosCFBundleIdentifier = "biz.nellemann.pwgen";
    }

    public void initUI() {
		SplashWindow sp;
		HomeView homeView = new HomeView();
		try {
			Images.loadImage();
			sp = new SplashWindow();
			sp.popupNonBlocking();
			swap(homeView);
		} catch (IOException | ImageException e) {
			e.printStackTrace();
		}
	}

}
